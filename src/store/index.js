import Vue from 'vue'
import Vuex from 'vuex'

export const getEventIndexById = (state, eventId) => state.events.findIndex(event => event.id.toString() === eventId.toString())

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    /* User */
    userName: null,
    userEmail: null,
    userAvatar: null,

    /* NavBar */
    isNavBarVisible: true,
    navBarColor: 'is-dark',

    /* FooterBar */
    isFooterBarVisible: false,

    /* Aside */
    isAsideVisible: true,
    isAsideExpanded: false,
    isAsideMobileExpanded: false,
    asideActiveForcedKey: null,
    isAsideRightVisible: false,
    isAsideRightActive: false,
    asideMenuColor: 'is-dark',

    /* Updates */
    hasUpdates: false,

    /* Overlay */
    isOverlayVisible: false,

    /* Layout */
    isLayoutBoxed: false,
    isLayoutAsideHidden: false,
    isLayoutMobile: false,

    /* Iframe */
    isIframePreviewMode: false,

    /* ConfigBox */
    isConfigBoxVisible: true
  },
  getters: {
    events: state => state.events,
    calendars: state => state.calendars,
    calendarSidebarOpened: state => state.calendarSidebarOpened,
    newdate: state => state.newdate
  },
  mutations: {
    /* A fit-them-all commit */
    basic (state, payload) {
      state[payload.key] = payload.value
    },

    /* User */
    user (state, payload) {
      if (payload.name) {
        state.userName = payload.name
      }
      if (payload.email) {
        state.userEmail = payload.email
      }
      if (payload.avatar) {
        state.userAvatar = payload.avatar
      }
    },

    /* Full Page mode */
    fullPage (state, payload) {
      state.isConfigBoxVisible = !payload
      state.isNavBarVisible = !payload
      state.isAsideVisible = !payload
      state.isFooterBarVisible = !payload
      state.isOverlayVisible = false
    },

    /* Aside Desktop Visibility */
    asideVisibilityToggle (state, payload) {
      state.isAsideVisible = payload
    },

    /* Aside Desktop State */
    asideStateToggle (state, payload = null) {
      const htmlAsideClassName = 'has-aside-expanded'
      const isExpand = payload !== null ? payload : !state.isAsideExpanded

      document.documentElement.classList[isExpand ? 'add' : 'remove'](htmlAsideClassName)

      state.isAsideExpanded = isExpand

      setTimeout(function () { window.dispatchEvent(new Event('resize')) }, 250)
    },

    /* Aside Mobile State */
    asideMobileStateToggle (state, payload = null) {
      const isShow = payload !== null ? payload : !state.isAsideMobileExpanded

      document.documentElement.classList[isShow ? 'add' : 'remove']('has-aside-mobile-expanded')

      state.isAsideVisible = true
      state.isAsideMobileExpanded = isShow
    },

    /* Aside Forced Active Key (when secondary submenu is open) */
    asideActiveForcedKeyToggle (state, payload) {
      state.asideActiveForcedKey = payload && payload.menuSecondaryKey ? payload.menuSecondaryKey : null
    },

    /* Aside Right */
    asideRightToggle (state, payload) {
      state.isAsideRightVisible = payload
      state.isAsideRightActive = payload
      state.hasUpdates = false
      setTimeout(function () { window.dispatchEvent(new Event('resize')) }, 250)
    },

    /* Overlay */
    overlayToggle (state, payload = null) {
      const setIsVisible = payload !== null ? payload : !state.isOverlayVisible

      if (!setIsVisible && state.isLayoutAsideHidden && (state.isAsideVisible || state.isAsideRightVisible)) {
        return
      }

      state.isOverlayVisible = setIsVisible

      document.documentElement.classList[setIsVisible ? 'add' : 'remove']('is-clipped')
    },

    /* Layouts */

    layoutBoxedToggle (state, payload = null) {
      const setIsBoxed = payload !== null ? payload : !state.isLayoutBoxed

      state.isLayoutAsideHidden = setIsBoxed
      state.isLayoutBoxed = setIsBoxed
      state.isAsideExpanded = setIsBoxed
      state.isAsideVisible = !setIsBoxed
      state.isAsideRightVisible = false
      state.isAsideRightActive = false

      document.documentElement.classList[setIsBoxed ? 'remove' : 'add']('has-aside-left', 'has-navbar-fixed-top')
      document.documentElement.classList[setIsBoxed ? 'add' : 'remove']('has-boxed-layout', 'has-aside-hidden-layout', 'has-aside-expanded')
    },

    layoutWideToggle (state, payload = null) {
      const setIsWide = payload !== null ? payload : !state.isLayoutBoxed

      state.isLayoutAsideHidden = setIsWide
      state.isAsideExpanded = setIsWide
      state.isAsideVisible = !setIsWide
      state.isAsideRightVisible = !setIsWide

      document.documentElement.classList[setIsWide ? 'remove' : 'add']('has-aside-left')
      document.documentElement.classList[setIsWide ? 'add' : 'remove']('has-aside-hidden-layout', 'has-aside-expanded')
    },

    layoutMobileToggle (state, payload) {
      state.isLayoutMobile = payload
    },

    /* Misc */

    setNavBarColor (state, payload) {
      state.navBarColor = payload
    },

    setAsideMenuColor (state, payload) {
      state.asideMenuColor = payload
    },

    iframePreviewMode (state, payload) {
      state.isIframePreviewMode = payload
    },

    /* FullCalendar */
    calendarRedraw (state, payload) {
      setTimeout(function () { window.dispatchEvent(new Event('resize')) }, 250)
    },
    CreateCalendarEvent (state, event) {
      return state.events.push(event)
    },

    UpdateCalendarEvent (state, updatedEvent) {
      const index = getEventIndexById(state, updatedEvent.id)

      if (index === -1) {
        return console.warn(`Unable to update event (id ${updatedEvent.id})`)
      }

      return state.events.splice(index, 1, {
        ...state.events[index],
        title: updatedEvent.title,
        start: updatedEvent.start,
        end: updatedEvent.end,
        allDay: updatedEvent.allDay,
        calendar: updatedEvent.extendedProps.calendar
      })
    },

    DeleteCalendarEvent (state, eventId) {
      const index = getEventIndexById(state, eventId)

      if (index === -1) {
        return console.warn(`Unable to delete event (id ${eventId})`)
      }

      return state.events.splice(index, 1)
    }
  },
  actions: {
    asideCloseAll ({ commit, state }) {
      commit('asideVisibilityToggle', false)
      commit('asideRightToggle', false)
      commit('overlayToggle', false)
    },
    asideVisibilityToggle ({ commit, state }, payload = null) {
      const setIsVisible = payload !== null ? payload : !state.isAsideVisible

      commit('asideVisibilityToggle', setIsVisible)
      commit('overlayToggle', setIsVisible)
    },
    asideRightToggle ({ commit, state }, payload = null) {
      const isShow = payload !== null ? payload : !state.isAsideRightVisible

      commit('asideRightToggle', isShow)

      if (state.isLayoutAsideHidden) {
        commit('overlayToggle', isShow)
      }

      if (!state.isLayoutAsideHidden) {
        document.documentElement.classList[isShow ? 'add' : 'remove']('has-aside-right')
      }
    },
    layoutMobileToggle ({ commit, state }) {
      const isMobile = window.innerWidth < 1024

      commit('layoutMobileToggle', isMobile)

      document.documentElement.classList[isMobile && state.isIframePreviewMode ? 'add' : 'remove']('iframe-preview-mode')
    },
    toggleFullPage ({ commit }, payload) {
      commit('layoutBoxedToggle', false)
      commit('fullPage', payload)

      document.documentElement.classList.remove('is-clipped')

      if (payload) {
        document.documentElement.classList.remove('has-aside-left', 'has-navbar-fixed-top')
      }
    },
    calendarSidebarToggle ({ commit }, payload) {
      return commit('calendarSidebarToggle', payload)
    },
    calendarChangeDate ({ commit }, date) {
      return commit('calendarChangeDate', date)
    },
    createEvent ({ commit }, event) {
      return commit('CreateCalendarEvent', event)
    },
    updateEvent ({ commit }, updatedEvent) {
      return commit('UpdateCalendarEvent', updatedEvent)
    },
    deleteEvent ({ commit }, eventId) {
      return commit('DeleteCalendarEvent', eventId)
    }
  }
})
