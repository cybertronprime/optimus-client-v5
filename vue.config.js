const fs = require('fs')

module.exports = {
  // Remove moment.js from chart.js
  // https://www.chartjs.org/docs/latest/getting-started/integration.html#bundlers-webpack-rollup-etc
  devServer: {
    host: 'adminone.optimus-avocats.fr',
    https: true,
    port: 8080,
    key: fs.readFileSync('/etc/letsencrypt/live/hub.cybertron.fr/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/hub.cybertron.fr/cert.pem')
  },
  configureWebpack: config => {
    return {
      externals: {
        moment: 'moment'
      }
    }
  }
}
